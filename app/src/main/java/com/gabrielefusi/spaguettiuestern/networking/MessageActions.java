package com.gabrielefusi.spaguettiuestern.networking;


public enum MessageActions {
    // Da client a server
    LOGIN,
    GAME_STATUS,
    PLAYER_MOVE,
    FIGHT_PERFORMED,
    // Da server a client
    ERROR,
    LOGIN_ACK,
    PLAYER_JOINED,
    PLAYER_EXITED,
    ROUND_START,
    ROUND_END,
    PLAYER_ROUND_START,
    PLAYER_ROUND_END,
    ENEMY_FOUND,
    FIGHT_START,
    FIGHT_END,
    UGLY_ENCOUNTERED,
    GAME_TIME,
    AMMO_FOUND,
    AMMO_UPDATED
}
