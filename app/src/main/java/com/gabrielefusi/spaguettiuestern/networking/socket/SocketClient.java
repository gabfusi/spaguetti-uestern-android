package com.gabrielefusi.spaguettiuestern.networking.socket;

import android.os.Process;

import com.gabrielefusi.spaguettiuestern.networking.MessageListener;
import com.gabrielefusi.spaguettiuestern.networking.MessageReceiver;
import com.gabrielefusi.spaguettiuestern.networking.MessageSender;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;

/**
 * Client networking
 */
public class SocketClient {
    private volatile Socket socket;
    private volatile MessageHandler handler;
    private volatile MessageSender sender;
    private boolean isListening = false;

    private String ip;
    private int port;

    private final Object listeningLock = new Object();

    private static SocketClient instance;

    /**
     * Simple implementation of singleton pattern
     * This acts like an android service.
     *
     * @return
     */
    public static synchronized SocketClient getInstance() {
        if (instance == null) {
            synchronized (SocketClient.class) {
                if (instance == null) {
                    try {
                        instance = new SocketClient(null, 8081, new MessageReceiver(), new MessageSender());
                    } catch (IOException e) {
                        // TODO notify error with an alert dialog

                    }
                }
            }
        }
        return instance;
    }

    private SocketClient(String ip, int port, MessageHandler handler, MessageSender sender) throws IOException {
        this.ip = ip;
        this.port = port;
        this.sender = sender;
        this.handler = handler;
        listen();
    }

    /**
     * Connect to server
     *
     * @throws IOException
     */
    private void connect() throws IOException {
        InetAddress address = ip == null ? InetAddress.getByName("10.0.2.2") : InetAddress.getByName(ip);
        socket = new Socket(address, port);
        sender.addSocket(this);
        System.out.println(this);
    }

    /**
     * Start a the listening Thread
     */
    private void listen() {
        listeningThread.setDaemon(true); // automatically shut down the thread on application exit
        listeningThread.start();
    }

    /**
     * Calls connect() and start listening
     */
    private final Thread listeningThread = new Thread(() -> {

        /*
         * Although it's more complicated than AsyncTask, you might want to instead create your own Thread or HandlerThread class.
         * If you do, you should set the thread priority to "background" priority by calling Process.setThreadPriority()
         * and passing THREAD_PRIORITY_BACKGROUND.
         * If you don't set the thread to a lower priority this way, then the thread could still slow down your app because
         * it operates at the same priority as the UI thread by default.
         */

        Process.setThreadPriority(THREAD_PRIORITY_BACKGROUND);

        synchronized (listeningLock) {

            isListening = true;
            BufferedReader reader;
            String message;

            try {

                connect();

                reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                while (isListening) {
                    message = reader.readLine();
                    if (message != null) {
                        handler.onReceive(this, message);
                    } else {
                        System.out.println("Seems that server shout down the connection...");
                        isListening = false;
                        close();
                    }

                }

            } catch (IOException e) {
                e.printStackTrace();
                // connection reset, server is down
            }
        }

    });

    /**
     * @param message
     */
    public synchronized void send(String message) { // should be invoked on a separate thread?
        PrintWriter writer;
        try {
            if (socket.isConnected()) {
                writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
                writer.println(message);
                System.out.println(Thread.currentThread().getId() + " - Socket.send: " + message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param messageListener
     */
    public synchronized void addListener(MessageListener messageListener) {
        handler.addListener(messageListener);
    }

    /**
     * @param messageListener
     */
    public synchronized void removeListener(MessageListener messageListener) {
        handler.removeListener(messageListener);
    }

    /**
     * Close networking connection
     */
    private synchronized void close() {
        try {
            if (socket != null && !socket.isClosed())
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @return
     */
    public synchronized MessageSender getSender() {
        return this.sender;
    }
}