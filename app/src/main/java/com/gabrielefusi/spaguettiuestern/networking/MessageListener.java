package com.gabrielefusi.spaguettiuestern.networking;

public interface MessageListener {
    void onSocketMessage(Message message);
}