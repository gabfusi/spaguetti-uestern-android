package com.gabrielefusi.spaguettiuestern.networking;

import com.gabrielefusi.spaguettiuestern.model.Team;
import com.gabrielefusi.spaguettiuestern.model.User;
import com.gabrielefusi.spaguettiuestern.networking.socket.SocketClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class MessageSender {

    private SocketClient socket;

    public MessageSender() {
    }

    public void addSocket(SocketClient socket) {
        this.socket = socket;
    }

    /**
     * Perform a login request
     *
     * @param nickname
     * @param team
     */
    public void login(String nickname, Team team) throws JSONException {
        // subscribe a player to next round

        Message message = new Message(MessageActions.LOGIN);
        JSONObject payload = new JSONObject();
        payload.put("nickname", nickname);
        payload.put("team", team);
        message.setPayload(payload);

        // create User singleton
        User.getInstance().setNickname(nickname);
        User.getInstance().setTeam(team);

        this.sendMessage(message);
    }

    /**
     *
     */
    public void requestGameStatus() throws JSONException {
        Message message = new Message(MessageActions.GAME_STATUS);
        message.setUserId(User.getInstance().getId());
        this.sendMessage(message);
    }

    /**
     * Perform a player move request
     *
     * @param playerId
     * @param cityId
     */
    public void playerMove(UUID playerId, UUID cityId) throws JSONException {
        // perform a player move to cityId

        Message message = new Message(MessageActions.PLAYER_MOVE);
        JSONObject payload = new JSONObject();
        payload.put("cityId", cityId);
        payload.put("playerId", playerId);
        message.setPayload(payload);

        this.sendMessage(message);
    }

    /**
     * @param playerId
     * @param answerId
     */
    public void fightPerformed(UUID playerId, int answerId) throws JSONException {
        // update player fight status

        Message message = new Message(MessageActions.FIGHT_PERFORMED);
        JSONObject payload = new JSONObject();
        payload.put("answerId", answerId);
        payload.put("playerId", playerId);
        message.setPayload(payload);

        this.sendMessage(message);

    }

    /**
     * @param message
     */
    private void sendMessage(Message message) throws JSONException {
        // convert Message to json
        String json = message.toJson();

        // return messages to connected client
        socket.send(json);
    }

}
