package com.gabrielefusi.spaguettiuestern.networking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class Message {
    private UUID userId = null;
    private MessageActions action;
    private JSONObject payload;

    public Message() {
    }

    Message(MessageActions action) {
        this.action = action;
        this.payload = new JSONObject();
    }

    public UUID getUserId() {
        return userId;
    }

    void setUserId(UUID userId) {
        this.userId = userId;
    }

    public MessageActions getAction() {
        return action;
    }

    public void setAction(MessageActions action) {
        this.action = action;
    }

    public JSONObject getPayload() {
        return payload;
    }

    void setPayload(JSONObject payload) {
        this.payload = payload;
    }

    String toJson() throws JSONException {
        JSONObject source = new JSONObject();
        if (this.userId != null) {
            source.put("userId", this.userId.toString());
        } else {
            source.put("userId", "anonymous");
        }
        source.put("action", this.action);
        source.put("payload", this.payload);
        return source.toString();
    }

    static Message fromJson(String json) throws JSONException {

        JSONObject source = new JSONObject(json);

        if (!source.has("action")) {
            throw new JSONException("Received unrecognized message, no action specified.");
        }

        if (!source.has("payload")) {
            throw new JSONException("Received unrecognized message, no payload specified.");
        }

        MessageActions action = MessageActions.valueOf(source.getString("action"));
        JSONObject payload = source.getJSONObject("payload");

        Message message = new Message(action);
        message.setPayload(payload);

        return message;
    }
}
