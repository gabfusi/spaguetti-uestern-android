package com.gabrielefusi.spaguettiuestern.networking.socket;

import com.gabrielefusi.spaguettiuestern.networking.MessageListener;

/**
 * Handler interface
 * onReceive is triggered when the router receive a new message
 */
public interface MessageHandler {
    void onReceive(SocketClient socket, String message);
    void addListener(MessageListener messageListener);
    void removeListener(MessageListener messageListener);
}