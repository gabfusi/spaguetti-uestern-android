package com.gabrielefusi.spaguettiuestern.networking;

import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.model.Player;
import com.gabrielefusi.spaguettiuestern.model.User;
import com.gabrielefusi.spaguettiuestern.networking.socket.MessageHandler;
import com.gabrielefusi.spaguettiuestern.networking.socket.SocketClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MessageReceiver implements MessageHandler {

    private List<MessageListener> listeners = new ArrayList<>();

    /**
     * On successful login
     *
     * @param payload
     */
    private void onLoginResponse(JSONObject payload) throws JSONException {

        // complete "registration"
        UUID userId = UUID.fromString(payload.getString("id"));
        User.getInstance().setId(userId);

        // set the players list
        // -> demanded to MatchmakingController
    }

    /**
     *
     * @param messagePayload
     * @throws JSONException
     */
    private void updateGameStatus(JSONObject messagePayload) throws JSONException {

        // players
        // update whole player list, some changes may happened server side.
        JSONArray players = messagePayload.getJSONArray("players");
        Game.getInstance().setPlayers(players);

        // world
        JSONObject world = messagePayload.getJSONObject("world");
        JSONArray cities = world.getJSONArray("cities");
        Game.getInstance().setCities(cities);

        // assign players to cities
        Game.getInstance().updatePlayersPosition();
    }

    /**
     * Perform one of the previous actions
     *
     * @param incomingMessage
     * @throws JSONException
     */
    private void performAction(Message incomingMessage) throws JSONException {

        JSONObject payload = incomingMessage.getPayload();

        switch (incomingMessage.getAction()) {

            case ERROR:
                System.out.println("TODO handle error! " + payload.get("message"));
                break;

            case GAME_TIME:
                // update game time
                Game.getInstance().setGameTime(payload.getLong("time"), payload.getLong("next_round_at"));
                break;

            case LOGIN_ACK:
                onLoginResponse(payload);
                break;

            case ROUND_START:
            case FIGHT_END:
            case PLAYER_ROUND_END:
            case PLAYER_MOVE:
                updateGameStatus(payload);
                break;

            case PLAYER_ROUND_START:
                Player player = Player.fromJson(payload);
                Game.getInstance().setCurrentPlayer(player);
                break;

            case PLAYER_EXITED:
            case PLAYER_JOINED:

                if (User.getInstance().getId().equals(UUID.fromString(payload.getString("id")))) {
                    return; // Do not notify this kind of events (quick fix)
                }

                break;
        }

        // Notify everybody that may be interested.
        for (MessageListener hl : listeners)
            hl.onSocketMessage(incomingMessage);

    }

    /**
     * @param connection the socket connection (Connection)
     * @param json       the received message
     */
    @Override
    public void onReceive(SocketClient connection, String json) {

        // check if message is a valid json
        try {

            //System.out.println("onReceive: received: " + json);

            Message incomingMessage = deserializeMessage(json);
            performAction(incomingMessage);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println("onReceive: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("onReceive: socket message is not a valid json. " + json);
        }

    }

    /**
     * Deserialize json string to Message instance
     *
     * @param message
     * @return
     * @throws JSONException
     */
    private Message deserializeMessage(String message) throws JSONException {
        if (message != null && !message.equals("") && !message.equals("null") && message.substring(0, 1).equals("{")) {
            return Message.fromJson(message);
        } else {
            return null;
        }
    }

    // implementing the observer pattern...

    @Override
    public void addListener(MessageListener messageListener) {
        listeners.add(messageListener);
    }

    @Override
    public void removeListener(MessageListener messageListener) {
        listeners.remove(messageListener);
    }
}
