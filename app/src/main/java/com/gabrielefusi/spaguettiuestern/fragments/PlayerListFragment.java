package com.gabrielefusi.spaguettiuestern.fragments;


import android.app.ListFragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.gabrielefusi.spaguettiuestern.R;
import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.model.Player;
import com.gabrielefusi.spaguettiuestern.model.User;
import com.gabrielefusi.spaguettiuestern.networking.Message;
import com.gabrielefusi.spaguettiuestern.networking.MessageListener;
import com.gabrielefusi.spaguettiuestern.networking.socket.SocketClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * A simple {@link Fragment} subclass.
 */
public class PlayerListFragment extends ListFragment implements MessageListener {

    private List<String> list = null;
    private ArrayAdapter<String> adapter;

    public PlayerListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_player_list, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        refreshList();
    }

    @Override
    public void onStart() {
        super.onStart();
        SocketClient.getInstance().addListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        SocketClient.getInstance().removeListener(this);
    }


    private void initList() {
        list = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
        setListAdapter(adapter);
    }

    private void refreshList() {

        if (list == null) {
            initList();
        }

        list.clear();

        for (Player player : Game.getInstance().getPlayers()) {
            String name;
            if(User.getInstance().getId().equals(player.getId())) {
                name = "YOU";
            } else {
                name = player.getNickname();
            }
            list.add(name + " (" + player.getTeam() + " Team)");
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onSocketMessage(Message message) {

        JSONObject payload = message.getPayload();

        System.out.println(Thread.currentThread().getId() + " - PlayerListFrag: onSocketMessage " + message.getAction() + " " + message.getPayload().toString());

        try {

            switch (message.getAction()) {

                case PLAYER_JOINED:
                    Player player = Player.fromJson(payload);
                    Game.getInstance().subscribePlayer(player);
                    refreshList();
                    break;

                case PLAYER_EXITED:
                    UUID playerId = UUID.fromString(payload.getString("id"));
                    Game.getInstance().unsubscribePlayer(playerId);
                    refreshList();
                    break;

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}
