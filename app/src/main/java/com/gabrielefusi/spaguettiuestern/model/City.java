package com.gabrielefusi.spaguettiuestern.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * Models a graph node
 */
public class City {

    private UUID id;
    private String name;
    private String image;
    private Hashtable<UUID, UUID> connections;
    private Hashtable<UUID, Player> players;
    private boolean visited = false;


    public UUID getId() {
        return id;
    }

    public City(UUID id, String name) {
        this.id = id;
        this.name = name;
        this.connections = new Hashtable<>();
        this.players = new Hashtable<>();
    }


    public void addConnection(UUID cityId) {
        this.connections.put(cityId, cityId);
    }

    public boolean isConnectedTo(City city) {
        return this.connections.get(city.getId()) != null;
    }

    public Collection<UUID> getConnections() {
        return connections.values();
    }

    public void addPlayer(Player player) {
        if (this.players.get(player.getId()) != null) {
            throw new RuntimeException("Player is already in " + getName() + " city.");
        }

        this.players.put(player.getId(), player);
        this.visited = true;
    }

    public void removePlayer(Player player) {
        if (this.players.get(player.getId()) == null) {
            throw new RuntimeException("Player isn't in " + getName() + " city.");
        }

        this.players.remove(player.getId());
    }

    public void clearPlayers() {
        players.clear();
    }

    public Collection<Player> getPlayers() {
        return players.values();
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isVisited() {
        return visited;
    }


    /**
     * Player factory from json
     *
     * @param json
     * @return
     */
    public static City fromJson(JSONObject json) {

        UUID cityId;
        String name;

        try {

            if (json.has("id") && json.has("name")) {

                cityId = UUID.fromString(json.getString("id"));

                name = json.getString("name");

                City newCity = new City(cityId, name);

                if (json.has("image")) {
                    newCity.setImage(json.getString("image"));
                }

                if (json.has("connections")) {
                    JSONArray cityConnections = json.getJSONArray("connections");

                    for (int j = 0; j < cityConnections.length(); j++) {
                        JSONObject connection = cityConnections.getJSONObject(j);
                        UUID connectionId = UUID.fromString(connection.getString("id"));
                        newCity.addConnection(connectionId);
                    }
                }

                return newCity;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
