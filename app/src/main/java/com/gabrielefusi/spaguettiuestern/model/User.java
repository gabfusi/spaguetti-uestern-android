package com.gabrielefusi.spaguettiuestern.model;

import java.util.UUID;

/**
 * User as a singleton
 */
public class User {

    private UUID id;
    private String nickname;
    private volatile Team team;
    private volatile UUID currentCityId;
    private volatile int ammo;

    /**
     * User is a singleton
     */
    private User() {
    }

    // using the  Initialization-on-demand holder idiom
    // see https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom

    private static class LazyHolder {
        static final User INSTANCE = new User();
    }

    public static User getInstance() {
        return LazyHolder.INSTANCE;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public UUID getCurrentCityId() {
        return currentCityId;
    }

    void setCurrentCityId(UUID currentCityId) {
        this.currentCityId = currentCityId;
    }

    public int getAmmo() {
        return ammo;
    }

    public void setAmmo(int ammo) {
        this.ammo = ammo;
    }
}
