package com.gabrielefusi.spaguettiuestern.model;

public enum Team {
    WHITE,
    BLACK,
    UGLY;
}