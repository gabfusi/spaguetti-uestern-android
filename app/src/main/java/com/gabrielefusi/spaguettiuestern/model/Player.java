package com.gabrielefusi.spaguettiuestern.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class Player {

    private UUID id;
    private String nickname;
    private Team team;

    private UUID currentCityId = null;
    private int ammo = 0;

    public Player(UUID id, String nickname, Team team) {
        this.id = id;
        this.nickname = nickname;
        this.team = team;
    }

    public UUID getId() {
        return id;
    }

    public String getNickname() {
        return nickname;
    }

    public Team getTeam() {
        return team;
    }

    public int getAmmo() {
        return ammo;
    }

    private void setAmmo(int ammo) {
        this.ammo = ammo;
    }

    public UUID getCurrentCityId() {
        return currentCityId;
    }

    public void setCurrentCityId(UUID id) {
        currentCityId = id;
    }

    /**
     * Player factory from json
     * @param json
     * @return
     */
    public static Player fromJson(JSONObject json) throws JSONException {

        UUID playerId;
        String nickname;
        Team team;
        Player newPlayer;

        if(json.has("id") && json.has("nickname") && json.has("team")) {

            playerId =  UUID.fromString(json.getString("id"));
            nickname = json.getString("nickname");
            team = Team.valueOf(json.getString("team"));
            newPlayer =  new Player(playerId, nickname, team);

            if(json.has("currentCityId")) {
                newPlayer.setCurrentCityId(UUID.fromString(json.getString("currentCityId")));
            }

            if(json.has("ammo")) {
                newPlayer.setAmmo(json.getInt("ammo"));
            }

            return newPlayer;

        }

        return null;
    }

}
