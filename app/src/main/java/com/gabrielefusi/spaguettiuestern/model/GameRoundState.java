package com.gabrielefusi.spaguettiuestern.model;

public enum GameRoundState {
    MATCHMAKING,
    IDLE,
    STARTED,
    ENDED;
}
