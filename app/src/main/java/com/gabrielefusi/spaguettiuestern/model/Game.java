package com.gabrielefusi.spaguettiuestern.model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;
import java.util.Hashtable;
import java.util.UUID;

public class Game {

    private Hashtable<UUID, Player> playerList = new Hashtable<>();
    private Hashtable<UUID, City> cityList = new Hashtable<>();
    private Player currentPlayer = null;

    private long gameTime = 0;
    private long nextRoundTime = 0;

    private final Object lockPlayers = new Object();
    private final Object lockCities = new Object();

    /**
     * Game is a singleton
     */
    private Game() {
    }

    // using the  Initialization-on-demand holder idiom
    // see https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom

    private static class LazyHolder {
        static final Game INSTANCE = new Game();
    }

    public static Game getInstance() {
        return LazyHolder.INSTANCE;
    }


    // time

    /**
     *
     * @param gameTime
     * @param nextRoundTime
     */
    public void setGameTime(long gameTime, long nextRoundTime) {
        this.gameTime = gameTime;
        this.nextRoundTime = nextRoundTime;
    }

    /**
     *
     * @return
     */
    public long getNextRoundTime() {
        return this.nextRoundTime - this.gameTime;
    }

    // players

    /**
     *
     * @param player
     */
    public void subscribePlayer(Player player) {
        playerList.put(player.getId(), player);
        System.out.println("Player " + player.getNickname() + " subscribed, total: " + playerList.size());
    }

    /**
     *
     * @param playerId
     */
    public void unsubscribePlayer(UUID playerId) {
        playerList.remove(playerId);
    }

    /**
     *
     * @return
     */
    public Collection<Player> getPlayers() {
        return playerList.values();
    }

    /**
     *
     * @param players
     * @throws JSONException
     */
    public void setPlayers(JSONArray players) throws JSONException {

        synchronized (lockPlayers) {
            playerList.clear();
            for (int i = 0; i < players.length(); i++) {
                Player player = Player.fromJson(players.getJSONObject(i));
                subscribePlayer(player);

                // update user
                if (User.getInstance().getId().equals(player.getId())) {
                    User.getInstance().setCurrentCityId(player.getCurrentCityId());
                    User.getInstance().setTeam(player.getTeam());
                    //User.getInstance().setAmmo(player.getAmmo());
                }
            }
        }

    }

    // cities

    /**
     *
     * @return
     */
    private Collection<City> getCities() {
        return cityList.values();
    }

    /**
     *
     * @param cities
     * @throws JSONException
     */
    public void setCities(JSONArray cities) throws JSONException {
        synchronized (lockCities) {
            cityList.clear();
            for (int i = 0; i < cities.length(); i++) {
                City city = City.fromJson(cities.getJSONObject(i));
                addCity(city);
            }
        }
    }

    /**
     *
     * @param cityId
     * @return
     */
    public City getCity(UUID cityId) {
        return cityList.get(cityId);
    }

    /**
     *
     * @param city
     */
    private void addCity(City city) {
        cityList.put(city.getId(), city);
    }

    /**
     *
     */
    public void updatePlayersPosition() {
        for (City city : getCities()) {
            city.clearPlayers();
            for (Player player : getPlayers()) {

                if (player.getCurrentCityId().equals(city.getId())) {
                    city.addPlayer(player);
                }
            }
        }
    }

    /**
     *
     * @param player
     */
    public void setCurrentPlayer(Player player) {
        currentPlayer = player;
    }

    /**
     *
     * @return
     */
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     *
     * @return
     */
    public boolean isUserTurn() {
        return currentPlayer != null && User.getInstance().getId() != null && currentPlayer.getId().equals(User.getInstance().getId());
    }
}
