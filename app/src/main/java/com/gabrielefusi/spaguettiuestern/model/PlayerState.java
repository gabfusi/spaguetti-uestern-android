package com.gabrielefusi.spaguettiuestern.model;

public enum PlayerState {
    WAITING,
    PLAYING,
    FIGHTING;
}