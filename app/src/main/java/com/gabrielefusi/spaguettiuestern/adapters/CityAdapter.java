package com.gabrielefusi.spaguettiuestern.adapters;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gabrielefusi.spaguettiuestern.R;
import com.gabrielefusi.spaguettiuestern.model.City;
import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.model.Player;
import com.gabrielefusi.spaguettiuestern.model.Team;
import com.gabrielefusi.spaguettiuestern.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class CityAdapter extends ArrayAdapter<City> {
    private Activity activity;
    private ArrayList<City> lCity;
    private static LayoutInflater inflater = null;

    public CityAdapter(Activity activity, int textViewResourceId, ArrayList<City> _lCity) {
        super(activity, textViewResourceId, _lCity);

        try {
            this.activity = activity;
            this.lCity = _lCity;

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {

        }
    }

    public int getCount() {
        return lCity.size();
    }

    @Override
    public City getItem(int position) {
        return lCity.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public TextView display_name;
        public TextView display_number;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        final ViewHolder holder;

        try {

            // DO not reuse item view.

            vi = inflater.inflate(R.layout.cities_list, null);
            holder = new ViewHolder();
            holder.display_name = (TextView) vi.findViewById(R.id.toptext);
            holder.display_number = (TextView) vi.findViewById(R.id.bottomtext);

            holder.display_name.setText(lCity.get(position).getName());
            holder.display_number.setText(getStringOfPlayersInsideCity(lCity.get(position)));

        } catch (Exception e) {
        }

        return vi;
    }

    private String getStringOfPlayersInsideCity(City city) {

        String cityInfo = "";
        Collection<Player> playersInCity = city.getPlayers();
        List<String> sb = new ArrayList<>();

        if (playersInCity.size() > 0) {

            int white = 0;
            int black = 0;

            for (Player p : playersInCity) {
                if (p.getTeam().equals(Team.BLACK)) {
                    black++;
                } else {
                    white++;
                }
            }

            if (black > 0) {
                sb.add(black + " Black Jackaroo");
            }
            if (white > 0) {
                sb.add(white + " White Jackaroo");
            }

            cityInfo = "(" + TextUtils.join(", ", sb) + ")";

        }

        return cityInfo;
    }
}