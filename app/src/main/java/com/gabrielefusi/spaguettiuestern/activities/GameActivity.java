package com.gabrielefusi.spaguettiuestern.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.gabrielefusi.spaguettiuestern.R;
import com.gabrielefusi.spaguettiuestern.adapters.CityAdapter;
import com.gabrielefusi.spaguettiuestern.model.City;
import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.model.Player;
import com.gabrielefusi.spaguettiuestern.model.Team;
import com.gabrielefusi.spaguettiuestern.model.User;
import com.gabrielefusi.spaguettiuestern.networking.Message;
import com.gabrielefusi.spaguettiuestern.networking.MessageListener;
import com.gabrielefusi.spaguettiuestern.networking.socket.SocketClient;
import com.golovin.fluentstackbar.FluentSnackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class GameActivity extends AppCompatActivity implements MessageListener {

    private FluentSnackbar notification;
    private TextView currentAmmoLabel;
    private TextView turnStatusLabel;
    private ListView citiesList;
    private TextView chooseCityLabel;
    private ArrayList<City> availableCities;
    private CityAdapter availableCitiesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // tabs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec("GAME_TAB");
        spec.setContent(R.id.gameTab).setIndicator("Game");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("PLAYERS_TAB");
        spec.setContent(R.id.playersTab).setIndicator("Players");
        tabHost.addTab(spec);

        notification = FluentSnackbar.create(this);
        currentAmmoLabel = (TextView) findViewById(R.id.currentAmmoLabel);
        turnStatusLabel = (TextView) findViewById(R.id.turnStatusLabel);
        citiesList = (ListView) findViewById(R.id.citiesList);
        chooseCityLabel = (TextView) findViewById(R.id.chooseCityLabel);

        // cities click
        citiesList.setOnItemClickListener(onCityListItemClick);

        showNotification("Game started!");

        updateUserAmmo();
        updateTurnStatus();
        refreshCitiesList();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SocketClient.getInstance().addListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SocketClient.getInstance().removeListener(this);
    }

    @Override
    public void onBackPressed() {}

    @Override
    public void onSocketMessage(Message message) {

        System.out.println(Thread.currentThread().getId() + " - Game: onSocketMessage " + message.getAction() + " " + message.getPayload().toString());

        try {

            JSONObject payload = message.getPayload();

            switch (message.getAction()) {

                case PLAYER_EXITED:
                    Game.getInstance().updatePlayersPosition();
                    break;

                case ROUND_START:
                    refreshCitiesList();
                    updateUserAmmo();
                    break;

                case PLAYER_ROUND_START:
                    updateTurnStatus();
                    break;

                case AMMO_FOUND:
                    int collectedAmmo = payload.getInt("ammo") - User.getInstance().getAmmo();
                    User.getInstance().setAmmo(payload.getInt("ammo"));
                    updateUserAmmo();
                    showNotification("Congrats! You collected " + collectedAmmo + " ammo!");
                    break;

                case AMMO_UPDATED:
                    int updatedAmmo = payload.getInt("ammo");
                    User.getInstance().setAmmo(updatedAmmo);
                    updateUserAmmo();
                    break;

                case ENEMY_FOUND:
                    showNotification("You found an enemy!");
                    break;

                case FIGHT_START:
                    showQuestionDialog(payload);
                    break;

                case FIGHT_END:

                    String status = payload.getString("winnerTeam");

                    if (status.equals("draw")) {

                        showNotification("No team win, both scored the same points!");

                    } else {

                        Team winnerTeam = Team.valueOf(status);

                        if (winnerTeam.equals(User.getInstance().getTeam())) {
                            showNotification("Your team won!");
                        } else {
                            showNotification(winnerTeam + " Team won!");
                        }
                    }

                    refreshCitiesList();
                    break;

                case UGLY_ENCOUNTERED:
                    int updatedUserAmmo = payload.getInt("ammo");
                    User.getInstance().setAmmo(updatedUserAmmo);
                    showNotification("The Ugly stole you 1 ammo!");
                    updateUserAmmo();
                    break;

                case PLAYER_ROUND_END:
                    System.out.println("Player round end -> " + payload.toString());
                    refreshCitiesList();
                    break;

                case PLAYER_MOVE:
                    refreshCitiesList();
                    break;

                case ROUND_END:
                    showNotification("Game Ended.");
                    showResults(payload);
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * On city list item click
     */
    private AdapterView.OnItemClickListener onCityListItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {

            City currentCity = Game.getInstance().getCity(User.getInstance().getCurrentCityId());
            City desiredCity = (City) adapter.getItemAtPosition(position);

            if (!currentCity.isConnectedTo(desiredCity)) {
                System.out.println("Cannot move to selected city!");
                return;
            }

            if (!Game.getInstance().isUserTurn()) {
                System.out.println("It isn't your turn!");
                return;
            }

            System.out.println("User want to travel to " + desiredCity.getName());

            try {
                SocketClient.getInstance().getSender().playerMove(User.getInstance().getId(), desiredCity.getId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     *
     */
    private void initCitiesList() {
        availableCities = new ArrayList<>();
        availableCitiesAdapter = new CityAdapter(this, R.layout.cities_list, availableCities);
        citiesList.setAdapter(availableCitiesAdapter);
    }

    /**
     *
     */
    private void refreshCitiesList() {

        if (availableCities == null) {
            initCitiesList();
        }

        availableCities.clear();

        City currentCity = Game.getInstance().getCity(User.getInstance().getCurrentCityId());

        for (UUID cityId : currentCity.getConnections()) {
            City connectedCity = Game.getInstance().getCity(cityId);
            availableCities.add(connectedCity);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                availableCitiesAdapter.notifyDataSetChanged();
            }
        });

    }

    /**
     * hide cities choice list
     */
    private void lockCitiesList() {
        citiesList.setVisibility(View.INVISIBLE);
        chooseCityLabel.setVisibility(View.INVISIBLE);
    }

    /**
     * show cities choice list
     */
    private void unlockCitiesList() {
        citiesList.setVisibility(View.VISIBLE);
        chooseCityLabel.setVisibility(View.VISIBLE);
    }

    /**
     *
     * @param qa
     * @throws JSONException
     */
    private void showQuestionDialog(JSONObject qa) throws JSONException {

        Context context = this;
        String question = qa.getString("question");
        JSONArray answers = qa.getJSONArray("answers");
        List<String> choices = new ArrayList<>();

        for (int i = 0; i < answers.length(); i++) {
            String answer = answers.getString(i);
            choices.add(answer);
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .title("Fight with your knowledge!")
                        .content(question)
                        .items(choices)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                int answerIndex = which;

                                try {
                                    SocketClient.getInstance().getSender().fightPerformed(User.getInstance().getId(), answerIndex);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                System.out.println("Your choice: " + text);

                                return true;
                            }
                        })
                        .positiveText("Choose")
                        .show();

            }
        });

    }

    /**
     * Update GUI on player turn change
     */
    private void updateTurnStatus() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (Game.getInstance().isUserTurn()) {
                    turnStatusLabel.setText("It's your turn!");
                    unlockCitiesList();
                    showNotification("Hurry up! It's your turn!");
                } else if(Game.getInstance().getCurrentPlayer() != null) {
                    turnStatusLabel.setText(String.format("It's %s turn!", Game.getInstance().getCurrentPlayer().getNickname()));
                    lockCitiesList();
                }

            }
        });
    }

    /**
     * Update GUI when User ammo changes
     */
    private void updateUserAmmo() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                currentAmmoLabel.setText(String.format("Ammos: %d", User.getInstance().getAmmo()));

            }
        });
    }

    /**
     * @param text
     */
    private void showNotification(String text) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                notification.create(text)
                        .maxLines(2) // default is 1 line
                        //.backgroundColorRes(R.color.purple_500) // default is #323232
                        //.textColorRes(R.color.blue_grey_500) // default is Color.WHITE
                        .duration(Snackbar.LENGTH_SHORT) // default is Snackbar.LENGTH_LONG
                        //.actionText("Action text") // default is "Action"
                        //.actionTextColorRes(R.color.colorAccent)
                        .important()
                        .show();
            }
        });

    }

    /**
     * Show game activity
     */
    private void showResults(JSONObject resultsPayload) {

        Intent results = new Intent(this, ResultsActivity.class);
        results.putExtra("resultsPayload", resultsPayload.toString());

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startActivity(results);
            }
        });
    }


}
