package com.gabrielefusi.spaguettiuestern.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;

import com.gabrielefusi.spaguettiuestern.R;
import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.model.Player;
import com.gabrielefusi.spaguettiuestern.model.Team;
import com.gabrielefusi.spaguettiuestern.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {


    private ArrayList<String> whitePlayers;
    private ArrayList<String> blackPlayers;
    private ArrayAdapter<String> whitePlayersAdapter;
    private ArrayAdapter<String> blackPlayersAdapter;
    private ListView whiteTeamList;
    private ListView blackTeamList;
    private TextView resultsIntroLabel;
    private TextView resultsDescriptionLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        // tabs
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec("STANDINGS_TAB");
        spec.setContent(R.id.standingsTab).setIndicator("Info");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("WHITE_TEAM_TAB");
        spec.setContent(R.id.whiteTeamTab).setIndicator("White team");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("BLACK_TEAM_TAB");
        spec.setContent(R.id.blackTeamTab).setIndicator("Black team");
        tabHost.addTab(spec);

        resultsIntroLabel = (TextView) findViewById(R.id.resultsIntroLabel);
        resultsDescriptionLabel = (TextView) findViewById(R.id.resultsDescriptionLabel);
        whiteTeamList = (ListView) findViewById(R.id.whiteTeamList);
        blackTeamList = (ListView) findViewById(R.id.blackTeamList);

        try {

            populateInfoTab();
            populateStandingsTabs();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {}

    /**
     * Populate info tab
     * @throws JSONException
     */
    private void populateInfoTab() throws JSONException {

        // retrieve ROUND_END payload
        JSONObject payload = new JSONObject(getIntent().getStringExtra("resultsPayload"));

        JSONArray players = payload.getJSONArray("players");
        Game.getInstance().setPlayers(players);

        JSONObject results = payload.getJSONObject("results");
        boolean isDraw = results.getBoolean("isDraw");
        int whiteTeamAmmo = results.getInt(Team.WHITE.toString());
        int blackTeamAmmo = results.getInt(Team.BLACK.toString());

        if (isDraw) {

            resultsIntroLabel.setText("DRAW.");
            resultsDescriptionLabel.setText(MessageFormat.format("Your team and the opposing team collected the same amount of ammo {0}.", whiteTeamAmmo));

        } else {

            Team winnerTeam = Team.valueOf(results.getString("winnerTeam"));
            Team loserTeam = winnerTeam.equals(Team.WHITE) ? Team.BLACK : Team.WHITE;
            int winnerTeamAmmo = whiteTeamAmmo > blackTeamAmmo ? whiteTeamAmmo : blackTeamAmmo;
            int loserTeamAmmo = whiteTeamAmmo > blackTeamAmmo ? blackTeamAmmo : whiteTeamAmmo;

            if (winnerTeam.equals(User.getInstance().getTeam())) {

                resultsIntroLabel.setText("Your Team Won!");
                resultsDescriptionLabel.setText(MessageFormat.format("Your team won {0} - {1} against {2} Team", winnerTeamAmmo, loserTeamAmmo, loserTeam));

            } else {
                resultsIntroLabel.setText("Your Team Lose.");
                resultsDescriptionLabel.setText(MessageFormat.format("Your team lose {0} - {1} against {2} Team", loserTeamAmmo, winnerTeamAmmo, winnerTeam));
            }

        }
    }


    /**
     *
     */
    private void populateStandingsTabs() {

        whitePlayers = new ArrayList<>();
        whitePlayersAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, whitePlayers);
        whiteTeamList.setAdapter(whitePlayersAdapter);

        blackPlayers = new ArrayList<>();
        blackPlayersAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, blackPlayers);
        blackTeamList.setAdapter(blackPlayersAdapter);

        // order players by ammo
        List<Player> players = new ArrayList<Player>(Game.getInstance().getPlayers());
        Collections.sort(players, new Comparator<Player>(){
            public int compare(Player obj1, Player obj2) {
                return Integer.valueOf(obj1.getAmmo()).compareTo(obj2.getAmmo()); // To compare integer values
            }
        });

        for (Player player : players) {

            if (player.getTeam().equals(Team.WHITE)) {
                whitePlayers.add(player.getNickname() + " (" + player.getAmmo() + " ammo)");
            } else {
                blackPlayers.add(player.getNickname() + " (" + player.getAmmo() + " ammo)");
            }

        }

        whitePlayersAdapter.notifyDataSetChanged();
        blackPlayersAdapter.notifyDataSetChanged();

    }

}
