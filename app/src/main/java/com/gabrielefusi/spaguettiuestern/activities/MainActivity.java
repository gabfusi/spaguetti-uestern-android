package com.gabrielefusi.spaguettiuestern.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gabrielefusi.spaguettiuestern.R;
import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.networking.socket.SocketClient;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize socket client connection
        SocketClient.getInstance();
        // initialize game
        Game.getInstance();

        // immediately show login screen
        Intent login = new Intent(this, LoginActivity.class);
        //overridePendingTransition(0,0);
        startActivity(login);

        // setContentView(R.layout.activity_main);
    }
}
