package com.gabrielefusi.spaguettiuestern.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gabrielefusi.spaguettiuestern.R;
import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.model.Player;
import com.gabrielefusi.spaguettiuestern.model.Team;
import com.gabrielefusi.spaguettiuestern.networking.Message;
import com.gabrielefusi.spaguettiuestern.networking.MessageListener;
import com.gabrielefusi.spaguettiuestern.networking.socket.SocketClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity implements MessageListener {

    private Button submitButton;
    private EditText nicknameField;
    private RadioGroup teamGroupField;
    private RadioButton whiteTeamField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nicknameField = (EditText) findViewById(R.id.nicknameField);
        teamGroupField = (RadioGroup) findViewById(R.id.teamGroupField);
        whiteTeamField = (RadioButton) findViewById(R.id.whiteTeamField);
        submitButton = (Button) findViewById(R.id.submitButton);

        // on login submit
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nickname = nicknameField.getText().toString();
                Team team;
                int selectedTeamId = teamGroupField.getCheckedRadioButtonId();
                RadioButton selectedTeam = (RadioButton) findViewById(selectedTeamId);

                if (validate(nickname, selectedTeam)) {

                    if (selectedTeam.equals(whiteTeamField)) {
                        team = Team.WHITE;
                    } else {
                        team = Team.BLACK;
                    }

                    System.out.println(Thread.currentThread().getId() + " - " + nickname + " - " + team);

                    try {
                        SocketClient.getInstance().getSender().login(nickname, team);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    showErrorDialog("Please enter a valid nickname and choose a team.");
                }
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        SocketClient.getInstance().addListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SocketClient.getInstance().removeListener(this);
    }

    @Override
    public void onBackPressed() {}

    /**
     * Executed on each socket message received
     *
     * @param message
     */
    @Override
    public void onSocketMessage(Message message) {

        System.out.println(Thread.currentThread().getId() + " - Login: onSocketMessage " + message.getAction() + " " + message.getPayload().toString());

        try {

            JSONObject payload = message.getPayload();

            switch (message.getAction()) {

                // on login error
                case ERROR:
                    showErrorDialog(payload.getString("message"));
                    break;

                // on login successful
                case LOGIN_ACK:

                    // update game time
                    Game.getInstance().setGameTime(payload.getLong("time"), payload.getLong("next_round_at"));

                    // update players list
                    JSONArray players = payload.getJSONArray("players");

                    // set players to game
                    for (int i = 0; i < players.length(); i++) {
                        JSONObject player = players.getJSONObject(i);
                        Game.getInstance().subscribePlayer(Player.fromJson(player));
                    }

                    // if login ok show matchmaking
                    showMatchmaking();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Show an error dialog
     * @param message
     */
    private void showErrorDialog(String message) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle("Login error")
                            .setMessage(message)
                            .setCancelable(true)
                            .setPositiveButton("Ok", (DialogInterface dialog, int id) -> dialog.cancel())
                            .show();
                }
            }
        });


    }

    /**
     * Show matchmaking activity
     */
    private void showMatchmaking() {

        Intent matchmaking = new Intent(this, MatchmakingActivity.class);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startActivity(matchmaking);
            }
        });

    }

    /**
     * Validate login
     * @param nickname
     * @param team
     * @return
     */
    private boolean validate(String nickname, RadioButton team) {
        return !nickname.isEmpty() && team != null;
    }
}
