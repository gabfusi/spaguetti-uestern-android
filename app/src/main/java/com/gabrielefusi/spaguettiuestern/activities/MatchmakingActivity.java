package com.gabrielefusi.spaguettiuestern.activities;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gabrielefusi.spaguettiuestern.R;
import com.gabrielefusi.spaguettiuestern.model.Game;
import com.gabrielefusi.spaguettiuestern.model.Player;
import com.gabrielefusi.spaguettiuestern.networking.Message;
import com.gabrielefusi.spaguettiuestern.networking.MessageListener;
import com.gabrielefusi.spaguettiuestern.networking.socket.SocketClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class MatchmakingActivity extends AppCompatActivity implements MessageListener {

    private long countdownTime;
    private TextView countdownLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matchmaking);

        // countdown
        countdownLabel = (TextView) findViewById(R.id.countdownLabel);
        adjustCountdown();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SocketClient.getInstance().addListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        SocketClient.getInstance().removeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCountdown();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopCountdown();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onSocketMessage(Message message) {

        //System.out.println(Thread.currentThread().getId() + " - Matchmaking: onSocketMessage " + message.getAction() + " " + message.getPayload().toString());

        switch (message.getAction()) {

            case GAME_TIME:
                adjustCountdown();
                break;

            case ROUND_START:
                showGame();
                break;

            case ERROR:

                try {
                    showError(message.getPayload().getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /**
     * Countdown timer task
     */
    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {

            if (countdownTime > 0) {
                countdownTime -= 1000;
                countdownLabel.setText(formatCountdownString(countdownTime));
            }

            timerHandler.postDelayed(this, 1000);
        }
    };

    /**
     * Start countdown timer
     */
    private void startCountdown() {
        timerHandler.postDelayed(timerRunnable, 0);
    }

    /**
     * Stop countdown timer
     */
    private void stopCountdown() {
        timerHandler.removeCallbacks(timerRunnable);
    }

    /**
     * Adjust countdown with server time
     */
    private void adjustCountdown() {
        this.countdownTime = Game.getInstance().getNextRoundTime();
    }

    private void showError(String message) {
        runOnUiThread(() -> {

            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(this);
            }

            builder.setTitle("Error")
                    .setMessage(message)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            backToLogin();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        });
    }

    /**
     * Format countdown string
     *
     * @param millis
     * @return
     */
    @SuppressLint("DefaultLocale")
    private String formatCountdownString(long millis) {
        return String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    /**
     * Show game activity
     */
    private void showGame() {

        Intent game = new Intent(this, GameActivity.class);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timerHandler.removeCallbacks(timerRunnable);
                startActivity(game);
            }
        });
    }

    /**
     * Show game activity
     */
    private void backToLogin() {

        Intent login = new Intent(this, LoginActivity.class);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timerHandler.removeCallbacks(timerRunnable);
                startActivity(login);
            }
        });
    }

}
